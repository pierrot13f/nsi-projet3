# -*- coding: utf-8 -*-
"""
Mini-projet choixpeau magique : trouver la maison de personnages à partir de leurs caractéristiques

Auteurs : VASSELET Thierno , LEVEBRE Salomé , FAURE Pierre

Licence (CR BR-NY-CA 2.0 FR)

VERSION remake

15/04/22
"""

import csv
from math import *

K = 5

# On importe les fichiers

with open('Characters.csv', mode='r', encoding='utf-8') as h:
    dictionnaires = csv.DictReader(h, delimiter=';')
    tab_characters = \
        [{key: values.replace('\xa0', ' ') for key,
          values in element.items()}for element in dictionnaires]

with open('Caracteristiques_des_persos.csv', mode='r', encoding='utf-8') as f:
    characteristics = csv.DictReader(f, delimiter=';')
    tab_characteristics = \
        [{key: values.replace('\xa0', ' ') for key,
          values in element.items()}for element in characteristics]

# Définition des fonctions

def affichage(tuples, dic):
    """
    Affiche les resultats.
    ----------
    Entrée: tuple et dictionnaire
    Sortie: chaine de caractères.
    """
    perso = ''
    txt = f'{tuples[0]}'
    num_p = 1
    for element in tuples[1]:
        if num_p == 1:
            perso += f"La maison du 1er voisin est {element[1]}," f"il s'appelle {dic[element[0]]}\n. "
            num_p += 1
        else:
            perso += f"La maison de votre {num_p}e voisin est {element[1]}," f" se prénommant {dic[element[0]]}\n. "
            num_p += 1
    return txt, perso

def house(index, perso): 
    """
    Trouve la maison d'un'perso 
    Donner la maison de ses voisins
    ----------
    Entrée: liste de dictionnaires et liste de tuples
    Sortie: chaine de caractère et liste de tuples 
    """
    gryf, rav, sly, huff = 0, 0, 0, 0
    house_k = []
    resultat = ''
    for voisin in perso:
        for element in index.items():
            if voisin[0] == element[0]:
                house_k.append(element)
    for neighboor in house_k:
        if neighboor[1] == 'Gryffindor':
            gryf += 1
        elif neighboor[1] == 'Ravenclaw':
            rav += 1
        elif neighboor[1] == 'Slytherin':
            sly += 1
        else:
            huff += 1
    liste_resultat = [('Gryffondor', gryf), ('Ravenclaw', rav), ('Slytherin', sly), ('Huffflepuff', huff)]
    liste_resultat.sort(key=lambda x: x[1], reverse = True)
    resultat = liste_resultat[0][0]
    return resultat, house_k

def kppv(k, tab):
    """
    Trouve les K plus proches voisins
    ----------
    Entrée: entier, liste et distance euclidienne
    Sortie: liste 
    """
    tab.sort(key=lambda x: x[1])
    proche = tab[:k]
    return proche

def distance_euclidienne(perso, index_perso):
    """
    Calcule la distance eucldienne
    ----------
    Entrée : liste de dictionnaires 
    Sortie : Une liste de tuples
    """
    voisins = []
    for i in index_perso.keys():
        distance = sqrt((perso['Courage'] - index_perso[i][0])**2 +
                        (perso['Ambition'] - index_perso[i][1])**2 +
                        (perso['Intelligence'] - index_perso[i][2])**2
                        + (perso['Good'] - index_perso[i][3])**2)
        voisins.append((i, distance))
    return voisins

# Fusion tables:

lit_tab = []    
for i in tab_characteristics:
    for j in tab_characters:
        if i['Name'] == j['Name']:
            i.update(j)
            lit_tab.append(i)

# Index

idx_stat = {int(perso['Id']): (int(perso['Courage']), int(perso['Ambition']), int(perso['Intelligence']), int(perso['Good'])) for perso in lit_tab}
idx_house = {int(perso['Id']): perso['House'] for perso in lit_tab}
idx_name = {int(perso['Id']): perso['Name'] for perso in lit_tab}
