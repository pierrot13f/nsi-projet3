"""
2e partie du projet 2 
Création d un site permettant de vous attribuer l une des quatres maisons de poudlard 
à travers un questionnaire magique !

Auteurs : VASSELET Thierno , LEVEBRE Salomé , FAURE Pierre

Licence (CR BR-NY-CA 2.0 FR)

15/04/22

"""

import profile
import KPPV
from browser import document as doc, html
import csv
from random import randint

# Importation questions 
tab_questions = []
with open('questions.csv', mode='r', encoding='utf-8') as h:
    lines = h.readlines()
    key_line = lines[0].strip()
    keys = key_line.split(';')
    for ligne_suivante in lines[1:]:
        ligne_suivante = ligne_suivante.strip() 
        valeurs = ligne_suivante.split(';')
        dico_questions = {}
        for i in range(len(keys)):
            dico_questions[keys[i]] = valeurs[i]
        tab_questions.append(dico_questions)

# Supprime les chaine de caractères autour des caractéristiques
for j in tab_questions:
    for i in range(1, 4): j[f'Caractéristiques{i}'] = (int(j[f'Caractéristiques{i}'][0]), int(j[f'Caractéristiques{i}'][2]), int(j[f'Caractéristiques{i}'][4]),int(j[f'Caractéristiques{i}'][6]))

# Variables pour les prochaines fonctions
num_question = 0
liste_rep = []

# Page d'accueil:
doc <= html.H1("QUELLE EST VOTRE MAISON DE PREDILECTION !?", id='titre')
doc <= html.IMG(src="chap.png",  id ='image_intro')
doc <= html.P("Alors comme ça tu ne sais pas encore à quelle maison tu appartiens !? Réponds honnetement à ce questionnaire et la maison à laquelle tu appartiens ne sera plus un SECRET ! Bonne chance !",  id='présentation')

# Bouton innital:
doc <= html.P(html.BUTTON("Démarer le test", id = "bouton_start"))

# Affichage des questions 
doc <= html.P(html.B(tab_questions[num_question]['Questions'], id = 'question'))
doc['question'].style.display ='none'

# Boutons réponses
for i in range(1, 4):
    doc <= (html.P(html.BUTTON(tab_questions[num_question][f'Réponses {i}'], id = f"boutton_rep{i}", value = i)))
    doc[f'boutton_rep{i}'].style.display = 'none'

# Bouton résultat
doc <= html.P(html.BUTTON("Affichez les resultats", id = 'result'))
doc['result'].style.display = 'none'

# Bouton pour choisir la valeur de K
k_buton = html.SELECT(html.OPTION(f'{i}') for i in range(5, 11))
choix_possible = [5, 6, 7, 8, 9, 10]
doc <= html.P("Choissisez la valeur de k:  ", id = 'choixk')
doc['choixk'] <= k_buton

# CORPS DU PROGRAMME 
def start(event): 
    """
    -Fait disparaître la page d'acceuil
    -Affiche le test.
    """
    doc['bouton_start'].style.display ='none'
    doc['titre'].style.display = 'none'
    doc['image_intro'].style.display = 'none'
    doc['présentation'].style.display = 'none'
    doc['choixk'].style.display = 'none'
    doc['question'].style.display = 'inline'
    for i in range(1, 4):
        doc[f'boutton_rep{i}'].style.display = 'inline'

doc['bouton_start'].bind("click", start)

def page_change(ev):
    """
    - Change les questions lorsque bouton "cliqué" 
    - Met ensuite les données 'stats' dans une liste
    """
    global num_question
    num_question += 1
    print(ev.target.value)

    liste_rep.append(tab_questions[num_question - 1][f'Caractéristiques{ev.target.value}'])
    if num_question != 16:#deja changé
        doc['question'].textContent = tab_questions[num_question]['Questions']
        for i in range(1, 4):
            doc[f'boutton_rep{i}'].textContent = tab_questions[num_question][f'Réponses {i}']
    else:
        doc['question'].style.display = 'none'
        for i in range(1, 4):
            doc[f'boutton_rep{i}'].style.display = 'none'
            doc['result'].style.display ='inline'   
for i in range(1, 4):
    doc[f'boutton_rep{i}'].bind("click", page_change)

def result(ev):
    """
    - Calcul du profil
    - Renvoi de la maison grâce au prgramme python 
    """
    doc['result'].style.display = 'none'

    courage, ambition, intelligence, good = 0, 0, 0, 0
    for i in liste_rep:
        courage += i[0]
        ambition += i[1]
        intelligence += i[2]
        good += i[3]
    profile = {'Courage': courage, 'Ambition': ambition,
               'Intelligence': intelligence, 'Good': good}

    voisins = KPPV.distance_euclidienne(profile, KPPV.idx_stat)
    kppv = KPPV.kppv(int(choix_possible[k_buton.selectedIndex]), voisins)
    maison = KPPV.affichage(KPPV.house(KPPV.idx_house, kppv), KPPV.idx_name)

    doc <= html.H3(f'Après un dilemme cornélien je vous attribue la maison -> {maison[0]} !')
    doc <= html.P(maison[1])
    if maison[0] == 'Gryffondor':
        doc <= html.IMG(src='Gryffondor.png', id='gryf')
    elif maison[0] == 'Slytherin':
        doc <= html.IMG(src='Slytherin.png', id='sly')
    elif maison[0] == 'Ravenclaw':
        doc <= html.IMG(src='Ravenclaw.png', id='rav')
    elif maison[0] == 'Hufflepuff':
        doc <= html.IMG(src='Hufflepuff.png', id='huf')


doc['result'].bind("click", result)




